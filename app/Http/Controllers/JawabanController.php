<?php

namespace App\Http\Controllers;

use App\Models\JawabanModel;
use Illuminate\Http\Request;

class JawabanController extends Controller
{
    public function store($id)
    {
        $isi = request()->input('jawaban');
        $tanggal_dibuat = date("Y:m:d H:i:s");
        $id_pertanyaan = $id;
        JawabanModel::create([
            'isi' => $isi,
            'tanggal_dibuat' => $tanggal_dibuat,
            'id_pertanyaan' => $id_pertanyaan
        ]);
        return redirect('/jawaban/' . $id);
    }
}
