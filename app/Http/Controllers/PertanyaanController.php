<?php

namespace App\Http\Controllers;

use App\Models\JawabanModel;
use App\Models\PertanyaanModel;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function index()
    {
        $result = PertanyaanModel::get_all();
        return view('pages/pertanyaan', ['result' => $result]);
    }

    public function create()
    {
        return view('pages/pertanyaanCreate');
    }

    public function store()
    {
        $judul = request()->input('judul');
        $isi = request()->input('isi');
        $tanggal_dibuat = date("Y:m:d H:i:s");

        PertanyaanModel::store(['isi' => $isi, 'judul' => $judul, 'tanggal_dibuat' => $tanggal_dibuat]);
        return redirect('pertanyaan/create');
    }

    public function detail($id)
    {
        $result = PertanyaanModel::get_once($id);
        $comment = JawabanModel::get_once($id);
        foreach ($result as $key) {
            return view('pages/pertanyaanDetail', ['result' => $key, 'comments' => $comment]);
        }
    }

    public function editPage($id)
    {
        $result = PertanyaanModel::get_once($id);
        foreach ($result as $key) {
            return view('pages/pertanyaanEdit', ['result' => $key]);
        }
    }

    public function edit($id)
    {
        $judul = request()->input('judul');
        $isi = request()->input('isi');
        $tanggal_diperbaharui = date("Y:m:d H:i:s");
        PertanyaanModel::update($id, ['judul' => $judul, 'isi' => $isi, 'tanggal_diperbaharui' => $tanggal_diperbaharui]);

        return redirect('jawaban/' . $id);
    }

    public static function delete($id)
    {
        PertanyaanModel::delete($id);
        return redirect('pertanyaan');
    }
}
