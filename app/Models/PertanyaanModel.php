<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class PertanyaanModel
{
    public static function get_all()
    {
        $items = DB::table('pertanyaan')->orderBy('tanggal_dibuat', 'desc')->get();
        return $items;
    }

    public static function store($data)
    {
        $items = DB::table('pertanyaan')->insert($data);
        return $items;
    }

    public static function get_once($id)
    {
        $item = DB::table('pertanyaan')->get()->where('id', "=", $id);
        return $item;
    }

    public static function update($id, $data)
    {
        DB::table('pertanyaan')->where('id', $id)->update($data);
    }

    public static function delete($id)
    {
        DB::table('pertanyaan')->where('id', '=', $id)->delete();
    }
}
