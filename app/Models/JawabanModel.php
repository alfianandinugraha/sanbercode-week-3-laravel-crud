<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class JawabanModel
{
    public static function create($data)
    {
        DB::table('jawaban')->insert($data);
    }

    public static function get_once($id)
    {
        $result = DB::table('jawaban')->where('id_pertanyaan', '=', $id)->get();
        return $result;
    }
}
