<?php

use Illuminate\Support\Facades\Route;

date_default_timezone_set("Asia/Jakarta");

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/home');
});

Route::get('/pertanyaan', 'PertanyaanController@index');

Route::get('/pertanyaan/create', 'PertanyaanController@create');

Route::post('/pertanyaan/create', 'PertanyaanController@store');

Route::get('/jawaban/{id}', 'PertanyaanController@detail');

Route::delete('/pertanyaan/{id}', 'PertanyaanController@delete');

Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@editPage');

Route::put('/pertanyaan/{id}/edit', 'PertanyaanController@edit');

Route::post('/jawaban/{id}', 'JawabanController@store');
