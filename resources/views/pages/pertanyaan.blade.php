@extends('../app')

@section('title', "Homepage")

@section('content')
<h1>List Pertanyan</h1>
<div class="list-group">
    @if(count($result) === 0)
    <h3>Tidak ada pertanyaan ~_~</h3>
    @else
    @foreach($result as $ask)
    <a href="<?= "./jawaban/" . $ask->id ?>" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1"><?= $ask->judul ?></h5>
            <small><?= $ask->tanggal_dibuat ?></small>
        </div>
        <p class="mb-1"><?= $ask->isi ?></p>
    </a>
    @endforeach
    @endif
</div>
@endsection
