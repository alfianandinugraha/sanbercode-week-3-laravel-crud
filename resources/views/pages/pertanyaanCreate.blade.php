@extends('../app')

@section('title', "Buat Pertanyaan")

@section('content')
<form action="./create" method="POST">
    @csrf
    <div class="form-group">
        <label for="titleInput">Judul</label>
        <input type="text" class="form-control" id="titleInput" name="judul">
    </div>
    <div class="form-group">
        <label for="isiInput">Isi</label>
        <textarea type="text" class="form-control" id="isiInput" rows="5" name="isi"></textarea>
    </div>
    <button class="btn btn-primary" type="submit">Buat Pertanyaan</button>
</form>
@endsection
