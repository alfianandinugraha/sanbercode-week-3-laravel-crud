@extends('../app')

@section('title', "Homepage")

@section('content')
<div class="pertanyaan">
    <h1><?= $result->judul ?></h1>
    <hr>
    <a href="<?= '/pertanyaan/' .  $result->id . '/edit' ?>" class="btn btn-primary">Edit</a>
    <form action="<?= '/pertanyaan/' . $result->id  ?>" method="POST" style="display: inline">
        @csrf
        <input name="_method" type="hidden" value="DELETE">
        <button type="submit" class="btn btn-danger">Hapus</button>
    </form>
    <hr>
    <i>Diposting <?= $result->tanggal_dibuat ?></i>
    <p><?= $result->isi ?></p>
</div>
<hr>
<div class="jawaban">
    <h3>Jawaban :</h3>
    @foreach($comments as $comment)
    <p><?= $comment->isi ?></p>
    @endforeach
</div>
<hr>
<form action="<?= "/jawaban/" . $result->id ?>" method="POST">
    @csrf
    <h3>Punya solusinya ? mari bantu jawab</h3>
    <div class="form-group">
        <label for="jawaban">Jawaban</label>
        <textarea class="form-control" id="jawaban" name="jawaban"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
