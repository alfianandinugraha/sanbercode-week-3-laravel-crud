@extends('../app')

@section('title', "Homepage")

@section('content')
<h1>Edit Pertanyaan</h1>

<form action="./edit" method="POST">
    @csrf
    <input name="_method" type="hidden" value="PUT">
    <div class="form-group">
        <label for="titleInput">Judul</label>
        <input type="text" class="form-control" id="titleInput" name="judul" value="<?= $result->judul ?>">
    </div>
    <div class="form-group">
        <label for="isiInput">Isi</label>
        <textarea type="text" class="form-control" id="isiInput" rows="5" name="isi"><?= $result->isi ?></textarea>
    </div>
    <button class="btn btn-primary" type="submit">Buat Pertanyaan</button>
</form>
@endsection
